from django.test import TestCase, Client

# Create your tests here.
class TestJadwal(TestCase):
    def test_login_exist(self):
        response = Client().get("/login/")
        self.assertEqual(response.status_code,200)

    def test_home_exist(self):
        response = Client().get("/")
        self.assertEqual(response.status_code,200)
