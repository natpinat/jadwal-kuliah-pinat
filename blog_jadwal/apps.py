from django.apps import AppConfig


class BlogjadwalConfig(AppConfig):
    name = 'blog_jadwal'
