from django.shortcuts import render, reverse, redirect
from django.views.generic import (ListView, DetailView, CreateView, DeleteView, UpdateView)
from .models import Jadwal
from django.urls import reverse_lazy

# Create your views here.

def home(request):
    context = {
        'jadwals': Jadwal.objects.all()
    }
    return render(request,"blog_jadwal/home.html", context)

def saying(request):
    return render(request,"blog_jadwal/sayhi.html")

class JadwalListView(ListView):
    model = Jadwal
    template_name ='blog_jadwal/home.html'
    context_object_name = 'jadwals'
    ordering = ['name']

class JadwalDetailView(DetailView):
    model = Jadwal

class JadwalCreateView(CreateView):
    model = Jadwal
    fields=['name','description','day','time','sks']

class JadwalUpdateView(UpdateView):
    model = Jadwal
    fields=['name','description','day','time','sks']

class JadwalDeleteView(DeleteView):
    model = Jadwal
    success_url = reverse_lazy("blog_jadwal:home")

def about(request):
    return render(request, "blog_jadwal/about.html",{ "title" : 'About', "subtitle" : 'About'})
