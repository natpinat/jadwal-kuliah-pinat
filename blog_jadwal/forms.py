from django import forms
from .models import Jadwal

class JadwalCreate(forms.Form):
    
    sks = forms.IntegerField()

    class Meta:
        model = Jadwal
        fields = ['name','description','day','time','sks']