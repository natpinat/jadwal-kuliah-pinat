"""jadwalKuliah URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from .views import (JadwalListView, JadwalDetailView, JadwalCreateView, JadwalUpdateView, JadwalDeleteView, saying)
from . import views

app_name='blog_jadwal'

urlpatterns = [
    path('', JadwalListView.as_view(), name= "home"),
    path("sayhi/",views.saying, name="sayhi"),
    path('jadwaldetail/<int:pk>',JadwalDetailView.as_view(), name= "jadwal-detail"),
    path('new', JadwalCreateView.as_view(), name = "jadwal-form"),
    path('jadwaldetail/<int:pk>/update',JadwalUpdateView.as_view(), name="jadwal-update"),
    path('jadwaldetail/<int:pk>/delete',JadwalDeleteView.as_view(), name="jadwal-delete"),
    path('about', views.about, name= "about"),
]
