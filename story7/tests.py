from django.test import LiveServerTestCase, TestCase, tag,Client

from django.urls import reverse, resolve
from .views import accordion

class TestViews(TestCase):
    def setUp(self) :
        self.client = Client()
        self.url_story7 = reverse("story7:accordion")

    def test_story7_GET(self):
        response = self.client.get(self.url_story7)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,"story7/what.html")
        self.assertContains(response,"Hello")

