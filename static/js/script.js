$(document).ready(function(){
    // inspirasi http://jsfiddle.net/sjnilan/ahj4J/104/
    $("#accordion").accordion();

    var selected=0;
    var selected2 = 0;
   
    var itemlist = $('#accordion'); // list accordion
    var len=$(itemlist).children().length; //length = tab x 2 (karena termasuk content) -- cth: tab ada 5 berarti length 10 
    
    $("h3").click(function(){
        selected= $(this).index(); // ambil index tab
        selected2 = $(this).index()+1; // ambil index content
    });
      

     $(".up").click(function(e){
        e.preventDefault();
        if(selected>0 && selected2>1){
            //geser posisi, bila sebelum pada posisi selected, sesudah pada posisi selected-2
               
            // untuk tab
            jQuery($(itemlist).children().eq(selected-2)).before(jQuery($(itemlist).children().eq(selected))); 
               
            // untuk content
            jQuery($(itemlist).children().eq(selected2-2)).before(jQuery($(itemlist).children().eq(selected2)));
            
            // update nilai
            selected=selected-2;
            selected2 = selected2-2;
     	}
    });

     $(".down").click(function(e){
        e.preventDefault();
        if(selected < len-1 && selected2 < len){
            // geser posisi, bila sebelum pada posisi selected sesudah pada posisi selected+2

            // untuk content
            jQuery($(itemlist).children().eq(selected2+2)).after(jQuery($(itemlist).children().eq(selected2)));
            
            // untuk tab
            jQuery($(itemlist).children().eq(selected+2)).after(jQuery($(itemlist).children().eq(selected)));
            
            // update nilai
            selected=selected+2;
            selected2 = selected2+2;
     	}
    });

});