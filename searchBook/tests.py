from django.test import LiveServerTestCase, TestCase, tag,Client

from django.urls import reverse, resolve
from .views import homeStory8, data

class TestViews(TestCase):
    def setUp(self) :
        self.client = Client()
        self.url_story8 = reverse("searchBook:home")

    def test_story8_GET(self):
        response = self.client.get(self.url_story8)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,"searchBook/home.html")
        self.assertContains(response,"Just type something to search!")

    def test_data(self):
        response = self.client.get("/story8/data?q=SherLock%20Holmes")
        self.assertContains(response,"Sherlock Holmes")
