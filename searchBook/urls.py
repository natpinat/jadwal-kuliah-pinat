from django.urls import path, include
from . import views

app_name='searchBook'

urlpatterns = [
    path('', views.homeStory8, name= "home"),
    path("data",views.data, name="data"),
]