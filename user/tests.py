from django.test import LiveServerTestCase, TestCase, tag,Client
from django.urls import reverse, resolve

from django.contrib.auth.models import User
from django.contrib.messages import get_messages
from django.contrib.auth.decorators import login_required

from .apps import UserConfig
from .views import register, profile
from .models import Profile

from selenium import webdriver

class UserConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(UserConfig.name,'user')

class TestModels(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("test",password='testpasstest123')
        self.new_user.save()
        self.profile = Profile.objects.create(
            user = self.new_user,
        )
    def test_instance_created(self):
        self.assertEqual(Profile.objects.count(),1)
    
    def test_instance_is_correct(self):
        self.assertEqual(Profile.objects.first().user,self.new_user)
    
    def test_to_string(self):
        self.assertIn("test",str(self.new_user.profile))


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.url_register = "/register/"
        self.url_login = "/login/"
        self.url_profile = "/profile/"
        self.user_new = User.objects.create_user("test2",password='testpasstest1232')
        self.user_new.save()
        self.profile = Profile.objects.create(user=self.user_new)

    def test_register_exist(self):
        response = self.client.get(self.url_register)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,"user/register.html")
    
    def test_login_exist(self):
        response = self.client.get(self.url_login)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,"user/login.html")

    def test_register_login_profile_POST(self):
        #register
        response = self.client.post(self.url_register, data = {
            "username" : "test",
            "email" : "test123@comp.com",
            "password1" : "testpasstest123",
            "password2" : "testpasstest123",
        })
        messages =list(get_messages(response.wsgi_request))
        self.assertEquals(str(messages[0]),'Account test created. Please Log In!')
        self.assertEquals(response.status_code,302)

        #test register same thing
        response = self.client.post(self.url_register, data = {
            "username" : "test2",
            "email" : "test123@comp.com",
            "password1" : "testpasstest123",
            "password2" : "testpasstest123",
        })
        messages =list(get_messages(response.wsgi_request))
        self.assertEquals(str(messages[1]),"Please read again password's criteria / Your password with password confirmation does not match")
        self.assertEquals(response.status_code,200)

        #try login
        response = self.client.post(self.url_login,data ={
            "username" : "test",
            "password" : "testpasstest123"
        })
        self.assertEquals(response.status_code,302)

class ProfileViewTest(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("test2",password='testpasstest1232')
        self.new_user.save()
        self.profile = Profile.objects.create(
            user = self.new_user        )
        self.response = self.client.login(
            username = "test2",
            password = "testpasstest1232"
        )

    def test_profile_GET(self):
        self.response = self.client.get("/profile/")
        self.assertTrue(200, self.response.status_code)
        self.assertTemplateUsed(self.response, 'user/profile.html')

