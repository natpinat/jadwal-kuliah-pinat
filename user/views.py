from django.shortcuts import render, redirect, reverse
from .forms import UserRegisterForm
from .models import Profile
from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required

def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            new_user = authenticate(
                username = username,
                password = form.cleaned_data['password1']
            )
            Profile.objects.create(user=new_user)
            
            messages.success(request, f'Account {username} created. Please Log In!')

            
            return redirect('user:login')
        else:
            form = UserRegisterForm(request.POST)
            messages.warning(request, "Please read again password's criteria / Your password with password confirmation does not match")
            return render(request,'user/register.html',{'form': form})
    else :
        form = UserRegisterForm()
        return render(request,'user/register.html',{'form': form})
        

@login_required
def profile(request):
    return render(request, 'user/profile.html')