from django.db import models
from django.shortcuts import reverse
# Create your models here.

class Orang(models.Model) :
    nama_orang = models.CharField(max_length=100, unique=True)

    def __str__(self) :
        return self.nama_orang

    def get_absolute_url(self):
        link_orang = self.nama_orang.replace(" ","-")
        return reverse("story6:detail_participant", args=[link_orang])

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=100, unique=True)
    nama_orang = models.ManyToManyField(Orang,blank=True)

    def __str__(self) :
        return self.nama_kegiatan
    
    def get_absolute_url(self):
        link_kegiatan = self.nama_kegiatan.replace(" ","-")
        return reverse("story6:detail_kegiatan", args=[link_kegiatan])
    