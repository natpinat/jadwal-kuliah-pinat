from django.shortcuts import render, redirect, reverse, get_object_or_404
from .forms import Add_Kegiatan, Add_Orang
from .models import Kegiatan, Orang
from django.contrib import messages

def index(request):
    context ={
        'kegiatan_obj' : Kegiatan.objects.all(),
        'orang_form' : Add_Orang,
    }
    return render(request, ('story6/index.html'), context)

def add_participant(request,id):
    kegiatan = Kegiatan.objects.get(id=id)
    if request.method == 'POST':
        nama_orang = request.POST["nama_orang"]

        orang_baru, status_new = Orang.objects.get_or_create(nama_orang=nama_orang)
        
        if status_new:
            orang_baru.save()
            kegiatan.nama_orang.add(orang_baru)
            messages.add_message(request,messages.SUCCESS,f"{nama_orang} successfully added to {kegiatan.nama_kegiatan}")
        
        else :
            try:
                kegiatan.nama_orang.get(nama_orang=nama_orang)
                messages.add_message(request, messages.WARNING, f"{nama_orang} has already in {kegiatan.nama_kegiatan}")
            except:
                kegiatan.nama_orang.add(orang_baru)
                messages.add_message(request, messages.SUCCESS, f"{nama_orang} successfully added {kegiatan.nama_kegiatan}")
            

    return redirect(reverse("story6:detail_kegiatan", args=[kegiatan.nama_kegiatan.replace(" ","-")]))

def detail_participant(request,nama):
    name = nama.replace("-"," ")
    orang = get_object_or_404(Orang,nama_orang=name)
    context ={
        'orang' : orang
    }
    return render(request,"story6/detail_participant.html", context)

def detail_kegiatan(request,nama):
    name = nama.replace("-"," ")
    kegiatan = get_object_or_404(Kegiatan,nama_kegiatan=name)
    context = {
        'kegiatan' : kegiatan,
        'orang_form' : Add_Orang,
    }
    return render(request,"story6/detail_kegiatan.html",context)

def add_kegiatan(request):
    if request.method == 'POST':
        kegiatan_forms = Add_Kegiatan(request.POST)
        if kegiatan_forms.is_valid :
            kegiatan_forms.save()
            tes = kegiatan_forms.cleaned_data.get('nama_kegiatan')
            auto = tes.replace(" ", "-")            
            messages.add_message(request,messages.SUCCESS,f"{tes} successfully added")
            return redirect(reverse("story6:detail_kegiatan",args=[auto]))
    
    context ={
        'kegiatan_forms' : Add_Kegiatan,
    }
    return render(request, ('story6/add_kegiatan.html'), context)


def delete_kegiatan(request,id):
    try :
        kegiatan = Kegiatan.objects.get(id=id)
        Kegiatan.objects.filter(id=id).delete()
        messages.add_message(request,messages.SUCCESS,f"{kegiatan.nama_kegiatan} deleted")
        return redirect("story6:index")

    except :
        messages.add_message(request,messages.WARNING,f"{kegiatan.nama_kegiatan} does not exist")
        return redirect("story6:index")

def delete_kegiatan_orang(request,nama,id):
    try :
        orang = Orang.objects.get(nama_orang=nama)
        kegiatan = Kegiatan.objects.get(id=id)
        kegiatan.nama_orang.remove(orang)
        messages.add_message(request,messages.SUCCESS,f"Participant {orang.nama_orang} leaves from {kegiatan.nama_kegiatan}")

    except :
        messages.add_message(request,messages.WARNING,f"{kegiatan.nama_kegiatan} does not exist")

    return redirect(reverse("story6:detail_participant", args=[orang.nama_orang.replace(" ","-")]))






    

