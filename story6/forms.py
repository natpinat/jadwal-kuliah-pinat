from django import forms
from .models import Kegiatan, Orang

class Add_Kegiatan(forms.ModelForm) :
    nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control",
    "placeholder" : "Nama Kegiatan"
    }), label = "")
    class Meta:
        model = Kegiatan
        exclude = ['nama_orang']

class Add_Orang(forms.ModelForm):
    nama_orang = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control",
        "placeholder" : "Nama Participant"
    }), label = "")
    class Meta:
        model = Orang
        fields = "__all__"