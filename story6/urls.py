from django.urls import path, include
from . import views

app_name='story6'

urlpatterns = [
    path('', views.index, name= "index"),
    path("participant/<str:nama>",views.detail_participant,name="detail_participant"),
    path("detail-kegiatan/<str:nama>",views.detail_kegiatan,name="detail_kegiatan"),
    path("add-participant/<int:id>",views.add_participant,name="add_participant"),
    path("add-kegiatan/",views.add_kegiatan,name="add_kegiatan"),
    path("delete/<int:id>", views.delete_kegiatan, name = "delete"),
    path("delete-participant/<str:nama>/<int:id>", views.delete_kegiatan_orang, name = "delete_participant"),
]