from django.test import LiveServerTestCase, TestCase, tag,Client
from .models import Kegiatan, Orang

from django.urls import reverse, resolve
from .views import index, add_participant, add_kegiatan, detail_kegiatan

from selenium import webdriver


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('story6:index'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

class TestModels(TestCase):
    def setUp(self):
        self.kegiatan_test = Kegiatan.objects.create(nama_kegiatan="kegiatan1")
        self.orang_test = Orang.objects.create(nama_orang="orang1")
        self.kegiatan_test.nama_orang.add(self.orang_test)
        
    def test_nama_kegiatan(self):
        self.assertEqual(str(self.kegiatan_test),"kegiatan1")

    def test_nama_orang(self):
        self.assertEqual(str(self.orang_test),"orang1")
    
    def test_orang_absolute_url(self):
        self.assertEquals(self.orang_test.get_absolute_url(),"/story6/participant/orang1")

    def test_kegiatan_absolute_url(self):
        self.assertEquals(self.kegiatan_test.get_absolute_url(),"/story6/detail-kegiatan/kegiatan1")


class TestViews(TestCase):
    def setUp(self) :
        self.client = Client()
        self.url_index = reverse("story6:index")

        self.kegiatan = Kegiatan.objects.create( nama_kegiatan = "kegiatan1")
        self.existOrang = Orang.objects.create(nama_orang="orang")
        
        self.url_detail_kegiatan = reverse("story6:detail_kegiatan", args=["kegiatan1"])
        self.url_detail_participant = reverse("story6:detail_participant",args=[self.existOrang.nama_orang.replace(" ","-")])        
        self.url_add_participant = reverse("story6:add_participant",args=[self.kegiatan.id])
        self.url_add_kegiatan = reverse("story6:add_kegiatan")
        
        self.toBeDelete_participant = Orang.objects.create(nama_orang="Sayo")
        self.toBeDelete_kegiatan = Kegiatan.objects.create(nama_kegiatan="Nara")
        self.toBeDelete_kegiatan.nama_orang.add(self.toBeDelete_participant)
        
        self.url_delete_participant = reverse("story6:delete_participant",args=[self.toBeDelete_participant.nama_orang,self.toBeDelete_kegiatan.id])
        self.url_delete_kegiatan = reverse("story6:delete",args=[self.toBeDelete_kegiatan.id])

    def test_index_GET(self):
        response = self.client.get(self.url_index)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,"story6/index.html")
        self.assertContains(response,"Kegiatan")

    def test_addKegiatan_GET(self):
        response = self.client.get(self.url_add_kegiatan)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,"story6/add_kegiatan.html")
        self.assertContains(response,"Add Kegiatan")

    def test_detailKegiatan_GET(self):
        response = self.client.get(self.url_detail_kegiatan)
        self.assertEquals(response.status_code,200)
        self.assertTemplateUsed(response,"story6/detail_kegiatan.html")
        self.assertContains(response,"Add Participant")
        self.assertContains(response,"kegiatan1")

    def test_detailParticipant_GET(self):
        response = self.client.get(self.url_detail_participant)
        self.assertTemplateUsed(response,"story6/detail_participant.html")
        self.assertContains(response,"orang")

    def test_addParticipant_POST(self):
        self.client.post(self.url_add_participant, {
            "nama_orang" : "orang",
            "nama_kegiatan" : "kegiatan",
        })
        response = self.client.post(self.url_add_participant, {
            "nama_orang" : "orang",
            "nama_kegiatan" : "kegiatan",
        })
        self.assertEquals(response.status_code,302)
        self.assertEquals(len(self.kegiatan.nama_orang.all()),1)

    def test_addParticipant_POST_two(self):
        self.client.post(self.url_add_participant,{
            "nama_orang" : "orang",
            "nama_kegiatan" : "kegiatan",
        })
        response = self.client.post(self.url_add_participant,{
            "nama_orang" : "wrongInput",
            "nama_kegiatan" : "wrongInput",
        },follow=True)
        self.assertEquals(response.status_code,200)
        self.assertContains(response,"Add Participant")

    def test_addParticipant_POST_three(self):
        self.client.post(self.url_add_participant,{
            "nama_orang" : "orang",
            "nama_kegiatan" : "kegiatan",
        })
        response = self.client.post(self.url_add_participant,{
            "nama_orang" : "new",
            "nama_kegiatan" : "kegiatan",
        },follow=True)
        self.assertEquals(response.status_code,200)
        self.assertContains(response,"Add Participant")

    def test_addKegiatan_POST_one(self):
        response = self.client.post(self.url_add_kegiatan, {
            "nama_kegiatan" : "kegiatan"
        })
        self.assertEquals(response.status_code,302)

    def test_addKegiatan_POST_two(self):
        response = self.client.post(self.url_add_kegiatan,{
            "nama_kegiatan" : "123456wrongInput"
        },follow=True)
        self.assertEquals(response.status_code,200)
        self.assertContains(response,"Add Kegiatan")


    def test_deleteKegiatanOrang_POST_one(self):
        self.kegiatan.nama_orang.add(self.toBeDelete_participant)
        length = len(self.kegiatan.nama_orang.all())
        response = self.client.post(self.url_delete_participant)
        self.assertEquals(len(self.kegiatan.nama_orang.all())-1, length-1)
        self.assertEquals(response.status_code,302)
    
    def test_deleteKegiatanOrang_POST_two(self):
        length = len(self.kegiatan.nama_orang.all())
        response = self.client.post(self.url_delete_participant,{
            "nama_orang" : "notfound",
            "nama_kegiatan" : "notfound",
        },follow=True)
        self.assertEquals(len(self.kegiatan.nama_orang.all())-1, length-1)
        self.assertEquals(response.status_code,200)
    

    def test_deleteKegiatan_POST_one(self):
        length = len(Kegiatan.objects.all())
        response = self.client.post(self.url_delete_kegiatan)
        self.assertEquals(len(Kegiatan.objects.all()),length-1)
        self.assertEquals(response.status_code,302)

    def test_deleteKegiatan_POST_two(self):
        length = len(Kegiatan.objects.all())
        response = self.client.post(self.url_delete_kegiatan,{
            "nama_kegiatan" : "notfound"
        }, follow=True)
        self.assertEquals(len(Kegiatan.objects.all()),length-1)
        self.assertEquals(response.status_code,200)
